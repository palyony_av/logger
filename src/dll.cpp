#include <Windows.h>
#include <nlohmann/json.hpp>

#include "include/pch.h"
#include "include/debug.h"
#include "include/database.h"

static std::unique_ptr<Database> g_pDatabase = nullptr;

#define AV_NOT_ENOUGH_MEMORY 2
#define AV_ERRONEOUS_INPUT 1
#define AV_SUCCESS 0

extern "C" __declspec(dllexport) uint32_t getAllLogEntries(void *pBuf, uint32_t bufSize, uint32_t* pUsedSize)
{
    Debug::log("In getAllLogEntries");
    if (pBuf == nullptr || pUsedSize == nullptr)
    {
        return AV_ERRONEOUS_INPUT;
    }

    std::forward_list<std::unique_ptr<Log>> logList = g_pDatabase->getAllLogEntries();

    std::string strResultJsonArr = "[";
    for (const std::unique_ptr<Log>& pLogMsg : logList)
    {
        strResultJsonArr += pLogMsg->toJson() + ',';
    }

    /* If in logList there are entries */
    if (strResultJsonArr.size() != 1)
    {
        strResultJsonArr[strResultJsonArr.size() - 1] = ']';
    } else {
        strResultJsonArr += ']';
    }
    
    /* We are need to use all json string size */
    *pUsedSize = static_cast<uint32_t>(strResultJsonArr.size());
    if (bufSize < *pUsedSize)
    {
        return AV_NOT_ENOUGH_MEMORY;
    }

    std::memcpy(pBuf, strResultJsonArr.c_str(), strResultJsonArr.size());
    Debug::log(strResultJsonArr);
    return AV_SUCCESS;
}

extern "C" __declspec(dllexport) void insertJsonEntry(const char * pJsonData, uint32_t jsonDataSize)
{
    bool isParseOk = false;
    Debug::log("Start to build log");
    
    std::unique_ptr<Log> pLog = LogNlohmannJson::buildLog(std::string(pJsonData, jsonDataSize));
    if (pLog == nullptr)
    {
        Debug::log("build fail");
        return;
    }

    Debug::log("build success");
    g_pDatabase->insertLogEntry(pLog);
    Debug::log("entry inserted");
}

BOOL APIENTRY DllMain(
    HANDLE hModule,	   // Handle to DLL module 
    DWORD ul_reason_for_call,
    LPVOID lpReserved)     // Reserved
{
    BOOL result = TRUE;

    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    // A process is loading the DLL.
    {
        g_pDatabase = Database::buildDatabaseWriter("log.db");
        if (g_pDatabase == nullptr)
        {
            result = FALSE;
        }
    }   break;

    case DLL_THREAD_ATTACH:
    // A process is creating a new thread.
    {

    }
    break;

    case DLL_THREAD_DETACH:
    // A thread exits normally.
    {
    }
    break;

    case DLL_PROCESS_DETACH:
    // A process unloads the DLL.
    {
        g_pDatabase.reset(nullptr);
    }   break;
    }
    return result;
}
