#ifndef LOGNLOHMANNJSON_H

#include <nlohmann/json.hpp>

#include "log.h"

class LogNlohmannJson :
	public Log
{
	using super = Log;
public:
	static std::unique_ptr<LogNlohmannJson> buildLog(const std::string& strJson);
	static std::unique_ptr<LogNlohmannJson> buildLog(
		unsigned int id,
		std::string event,
		std::string date,
		std::string time);
	~LogNlohmannJson() = default;

	std::string toJson();

private:
	LogNlohmannJson(unsigned int inId,
		std::string& inEvent,
		std::string& inDate,
		std::string& inTime);
};


#endif // !LOGNLOHMANJSON_H

