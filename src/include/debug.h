#ifndef DEBUG_H
#define DEBUG_H

#include <string>

namespace Debug
{
#ifdef _DEBUG
void log(const std::string&);
void log(const std::wstring&);
#else
void log(const std::string&) {}
void log(const std::wstring&) {}
#endif // NDEBUG	
}

#endif // DEBUG_H
