#ifndef DATABASE_H
#define DATABASE_H

#include "pch.h"
#include "logNlohmannJson.h"
#include "sqlite3.h"

typedef struct sqlite3 sqlite3;

class Database final
{
private:
    sqlite3* pSqliteDB;
    mutable std::mutex m_mutex;

    Database(sqlite3* pOpenedSqliteDB);
    Database(Database const&) = delete;
    Database& operator= (Database const&) = delete;

public:
    static std::unique_ptr<Database> buildDatabaseWriter(const char* databaseName);
    ~Database();

    bool insertLogEntry(const std::unique_ptr<Log>& pLog);
    std::forward_list<std::unique_ptr<Log>> getAllLogEntries();
};

#endif // DATABASE_H
