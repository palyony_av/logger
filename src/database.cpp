#include "include/database.h"

std::unique_ptr<Database> Database::buildDatabaseWriter(const char* databaseName)
{
    sqlite3 *pSqliteDB = nullptr;

    int res = sqlite3_open(databaseName, &pSqliteDB);
    if (res != SQLITE_OK)
    {
        return nullptr;
    }

    const char* pCommandCreateTable =
        "CREATE TABLE IF NOT EXISTS `Log` (\
            `id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\
            `event` TEXT NOT NULL,\
            `date` TEXT NOT NULL,\
            `time` TEXT NOT NULL\
        );";
    res = sqlite3_exec(pSqliteDB, pCommandCreateTable, nullptr, nullptr, nullptr);

    if (res != SQLITE_OK)
    {
        sqlite3_close(pSqliteDB);
        return nullptr;
    }

    return std::unique_ptr<Database>(new Database(pSqliteDB));
}

Database::Database(sqlite3* pOpenedSqliteDB)
    : pSqliteDB(pOpenedSqliteDB)
{

}

Database::~Database()
{
    if (this->pSqliteDB != nullptr)
    {
        sqlite3_close(this->pSqliteDB);
    }
}

bool Database::insertLogEntry(const std::unique_ptr<Log>& pLog)
{
    /* Acquire mutex */
    std::lock_guard<std::mutex> lock(this->m_mutex);

    sqlite3_stmt* stmt;

    sqlite3_prepare_v2(this->pSqliteDB, "INSERT INTO `Log` (event, date, time) VALUES (?1, ?2, ?3);", -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, pLog->getEvent().c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, pLog->getDate().c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 3, pLog->getTime().c_str(), -1, SQLITE_STATIC);

    int res = sqlite3_step(stmt);

    if (res != SQLITE_DONE)
    {
        sqlite3_finalize(stmt);
        return false;
    }

    sqlite3_finalize(stmt);
    return true;

    /* Mutex is automatically released when lock */
}

std::forward_list<std::unique_ptr<Log>> Database::getAllLogEntries()
{
    std::forward_list<std::unique_ptr<Log>> log_list = std::forward_list<std::unique_ptr<Log>>();
    sqlite3_stmt* stmt;
    std::lock_guard<std::mutex> lock(this->m_mutex);

    sqlite3_prepare_v2(this->pSqliteDB, "SELECT * FROM `Log` ORDER BY id;", -1, &stmt, NULL);

    int rc = sqlite3_step(stmt);

    if (rc == SQLITE_DONE)
    {
        sqlite3_finalize(stmt);
        return std::forward_list<std::unique_ptr<Log>>();
    }

    while (rc == SQLITE_ROW)
    {
        log_list.push_front(
            LogNlohmannJson::buildLog(
                (unsigned int)sqlite3_column_int(stmt, 0),          // id
                std::string((char*)sqlite3_column_text(stmt, 1)),   // event
                std::string((char*)sqlite3_column_text(stmt, 2)),   // date
                std::string((char*)sqlite3_column_text(stmt, 3))    // time
            )
        );

        rc = sqlite3_step(stmt);
    }

    sqlite3_finalize(stmt);

    return log_list;
}
