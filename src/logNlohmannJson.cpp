//#include "include/debug.h"
#include "include/logNlohmannJson.h"

std::unique_ptr<LogNlohmannJson> LogNlohmannJson::buildLog(const std::string& strJson)
{
    unsigned int id;
    std::string event;
    std::string date;
    std::string time;
    
    bool isParseOk;
    nlohmann::json jMsg;
    //Debug::log(strJson);
    try
    {
      //  Debug::log("Parse json");
        jMsg = nlohmann::json::parse(strJson);
        //Debug::log("Parse id");
        id = jMsg["id"].get<unsigned int>();
        //Debug::log("Parse event");
        event = jMsg["event"].get<std::string>();
        //Debug::log("Parse date");
        date = jMsg["date"].get<std::string>();
        //Debug::log("Parse time");
        time = jMsg["time"].get<std::string>();
        isParseOk = true;
    }
    /* Freaky way but it could not catch the exception */
    catch (...)
    {
        isParseOk = false;
    } 

    //Debug::log("After exception block");

    if (!isParseOk)
    {
        return nullptr;
    }

    //Debug::log("Parse is ok");

    return std::unique_ptr<LogNlohmannJson>(new LogNlohmannJson(id, event, date, time));
}

std::unique_ptr<LogNlohmannJson> LogNlohmannJson::buildLog(
    unsigned int id,
    std::string event,
    std::string date,
    std::string time)
{
    return std::unique_ptr<LogNlohmannJson>(
        new LogNlohmannJson(id, event, date, time)
        );
}

LogNlohmannJson::LogNlohmannJson(unsigned int inId,
    std::string& inEvent,
    std::string& inDate,
    std::string& inTime)
    :
    super(inId, inEvent, inDate, inTime)

{

}

std::string LogNlohmannJson::toJson()
{
    nlohmann::json jMsg;
    jMsg["id"] = LogNlohmannJson::super::getId();
    jMsg["event"] = LogNlohmannJson::super::getEvent();
    jMsg["date"] = LogNlohmannJson::super::getDate();
    jMsg["time"] = LogNlohmannJson::super::getTime();

    return jMsg.dump();
}
